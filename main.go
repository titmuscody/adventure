package main

import (
	"fmt"
	"os"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"

	"github.com/titmuscody/adventure/adventure"
)

func gameHandler(c *gin.Context) {
	action := c.Param("action")
	game := adventure.GetGame("")
	if action == "player" {
		c.JSON(http.StatusOK, game.Player)
	} else if action == "map" {
		c.JSON(http.StatusOK, gin.H{"map": game.Map})
	} else if action == "move" {
	} else {
		c.JSON(http.StatusNotFound, gin.H{"status": "command not recognized: " + action})
	}
}

func gamePostHandler(c *gin.Context) {
	action := c.Param("action")
	id := 1
	game := adventure.GetGame("")
	if action == "move" {
		loc := adventure.Point{}
		c.BindJSON(&loc)
		game.Move(id, loc.X, loc.Y)
		c.JSON(http.StatusOK, gin.H{"status": "success"})
	} else {
		c.JSON(http.StatusNotFound, gin.H{"status": "command not recognized: " + action})
	}
}

func socketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Failed to upgrade web socket: %+v", err)
	}
	// create a new game for attached client
	game := adventure.Game{}
	game.Init(9, 9)
	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			fmt.Println("exiting websocket due to error: %+v", err)
			break
		}
		fmt.Println("returning message %s", msg)
		conn.WriteMessage(t, msg)
fmt.Println("sending message back")
conn.WriteMessage(websocket.TextMessage, []byte("{\"location\": {\"x\":7, \"y\":4}}"))
	}
}
var wsupgrader = websocket.Upgrader{
	ReadBufferSize: 1024,
	WriteBufferSize: 1024,
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	r := gin.Default()

	r.GET("/game/:id/:action", gameHandler)
	r.POST("/game/:id/:action", gamePostHandler)

	r.GET("/ws", func(c *gin.Context) {
		socketHandler(c.Writer, c.Request)
	})

	r.Run(":" + port)
}
