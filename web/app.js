PIXI = require('pixi.js');
var game = require('./adventureGame.js');
var stage = new PIXI.Container();
var screensize = 400;
var renderer = PIXI.autoDetectRenderer(screensize,screensize);
var w = window.innerWidth;
var h = window.innerHeight;
var eventstarted = false;

function checkEvent(){
	if(eventstarted == false) {
		if(game.events.length > 0) {
			var event = game.events[0];
			event.run();
			game.events = game.events.splice(1);
		}
	}
}
if(w < h) {
	renderer.view.style.width = renderer.view.style.height = w;
} else {
	renderer.view.style.width = renderer.view.style.height = h;
}
var tilesize = screensize / game.level[0].length;
document.body.appendChild(renderer.view);
var cowgirl;
PIXI.loader.add('background', 'images/background/background.json').load(function(loader, res){
	var grassText = new PIXI.Texture.fromFrame("grass.png");
	var blueText = new PIXI.Texture.fromFrame("blue hearts.png");
	for(var i = 0; i < game.level.length; i++) {
		for(var j = 0; j < game.level[0].length; j++){
			var sprite;
			//console.log('trying ', i, j);
			if(game.level[i][j] === 1){
				sprite = new PIXI.Sprite(grassText);
			} else {
				sprite = new PIXI.Sprite(blueText);
			}
			sprite.height = sprite.width = tilesize;
			sprite.position.x = tilesize * j;
			sprite.position.y = tilesize * i;
			sprite.interactive = true;
			var move = function(eventdata){
				var x = eventdata.target.position.x;
				var y = eventdata.target.position.y;
				console.log('adding', x, y);
				
				game.events.push({run: function(){
					//console.log('moving y', eventdata);
					
					var sprx = game.character.sprite.position.x;
					var spry = game.character.sprite.position.y;
					stage.removeChild(game.character.sprite);
					if(spry - y > 0){
						game.character.sprite = game.character.sprites.up;
					} else {
						game.character.sprite = game.character.sprites.down;
					}
					stage.addChild(game.character.sprite);
					console.log(game.character.sprites);
					game.character.sprite.position.x = sprx;
					game.character.sprite.position.y = spry;
					tween(game.character.sprite.position, game.character.sprite.position.x, y, .25);
					}
				});
				
				
				game.events.push({run: function(){
					//console.log('moving', eventdata);
					
					var sprx = game.character.sprite.position.x;
					var spry = game.character.sprite.position.y;
					stage.removeChild(game.character.sprite);
					if(x - sprx > 0){
						game.character.sprite = game.character.sprites.right;
					} else {
						game.character.sprite = game.character.sprites.left;
					}
					stage.addChild(game.character.sprite);
					game.character.sprite.position.x = sprx;
					game.character.sprite.position.y = spry;
					tween(game.character.sprite.position, x, game.character.sprite.position.y, .25);
					}
				});
				
			}
			sprite.mousedown = move;
			sprite.touchstart = move;
			stage.addChild(sprite);
		}
	}
});

PIXI.loader.add('cowgirl', 'images/cowgirl/cowgirl.json').load(function(loader, res){
	var frames = [];
	cowgirl = {};
	for(var i = 0; i < 4; i++) {
		text = new PIXI.Texture.fromFrame("cowgirlwalkleft" + i + ".png");
		frames.push(text);
	}
	cowgirl.left = new PIXI.extras.MovieClip(frames);
	frames = [];
	for(var i = 0; i < 4; i++) {
		text = new PIXI.Texture.fromFrame("cowgirlwalkright" + i + ".png");
		frames.push(text);
	}
	cowgirl.right = new PIXI.extras.MovieClip(frames);
	cowgirl.up = new PIXI.extras.MovieClip([new PIXI.Texture.fromFrame("cowgirlwalkup0.png")]);
	cowgirl.down = new PIXI.extras.MovieClip([new PIXI.Texture.fromFrame("cowgirlwalkdown0.png")]);
	cowgirl.left.animationSpeed = cowgirl.right.animationSpeed = .2;
	cowgirl.left.play();
	cowgirl.right.play();
	cowgirl.left.height = cowgirl.right.height = cowgirl.left.width = cowgirl.right.width = cowgirl.up.width = cowgirl.up.height = cowgirl.down.height = cowgirl.down.width = tilesize;
	cowgirl.left.position.x = cowgirl.left.position.y = cowgirl.right.position.x = cowgirl.right.position.y = 0;
	game.character.sprites = cowgirl;
	game.character.sprite = cowgirl.right;
	stage.addChild(game.character.sprite);
	animate();
});
/*
setInterval(function(){
	var x = Math.floor((Math.random() * 1000) % 3) * tilesize;
	var y = Math.floor((Math.random() * 1000) % 3) * tilesize;
	game.events.push({run: function(){
		tween(game.character.sprite.position, x, game.character.sprite.position.y, .25);}
		});
	game.events.push({run: function(){
		tween(game.character.sprite.position, game.character.sprite.position.x, y, .25);}
	});
	}, 1000);
*/
function tween(pos, x, y, sec) {
	eventstarted = true;
	var count = Math.floor(sec * 30);
	var stepx = Math.floor((pos.x - x) / count);
	var stepy = Math.floor((pos.y - y) / count);
	var startPoint = pos.x;
	var origy = pos.y;
	//console.log('stepping to ', x, y, 'with', count, stepx, 'from', pos.x);
	for(var i = 0; i < count; i++){
		setTimeout(function(pos, i, stepx, stepy){
			pos.x = startPoint - (stepx * i);
			//console.log('new pos', pos.x, stepx, count);
			pos.y = origy - (stepy * i);
		},
		i * 50, pos, i, stepx, stepy);
	}
	setTimeout(function(pos, x, y, callback){
		pos.x = x;
		pos.y = y;
		var sprx = game.character.sprite.position.x;
		var spry = game.character.sprite.position.y;
		stage.removeChild(game.character.sprite);
		game.character.sprite = game.character.sprites.down;
		stage.addChild(game.character.sprite);
		game.character.sprite.position.x = sprx;
		game.character.sprite.position.y = spry;
		callback();
	},
	(count * 50), pos, x, y, function(){eventstarted = false;});
	/*
	if(callback != undefined){
		callback();
	}
	*/
}
	
function animate() {
	requestAnimationFrame(animate);
	checkEvent();
	renderer.render(stage);
};