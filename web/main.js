imageDatabase = {}
imageDatabase.load = function(code) {
	url = '/web/image/' + code + '.png';
	var img = new Image();
	img.onload = function(){
		imageDatabase[code] = img;
	};
	img.src = url;
}
imageDatabase.get = function(code) {
	if(code in imageDatabase) {
		return imageDatabase[code];
	} else {
		return null;
	}
}

function get(url) {
	req = new XMLHttpRequest();
	req.open("GET", url, false);
	req.send(null);
	return req.responseText;
}
var game;

function main()
{
	canvas = document.getElementById('canvas');
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
	if(game == null) {
		game = JSON.parse(get('/api/game'));
	}
	map = game.Map;
	if(map != null) {
		tilewidth = Math.floor(canvas.width / map[0].length);
		tileheight = Math.floor(canvas.height / map[0].length);
		if(tilewidth <= tileheight) {
			tilesize = tilewidth;
		} else {
			tilesize = tileheight;
		}
	}
	c = canvas.getContext("2d");
	for(i = 0; i < map.length; i++) {
		for(j = 0; j < map[i].length; j++) {
			if(map[i][j] == 2) {
				var tile = imageDatabase.get(1);
				if(tile != null) {
					x = j * tilesize;
					y = i * tilesize;
					c.drawImage(tile, x, y, tilesize, tilesize);
				} else {
					imageDatabase.load(1);
				}
			}
			var tile = imageDatabase.get(map[i][j]);
			if(tile != null) {
				x = j * tilesize;
				y = i * tilesize;
				c.drawImage(tile, x, y, tilesize, tilesize);
			} else {
				imageDatabase.load(map[i][j]);
			}
		}
	}
}

