package adventure
import (
	"github.com/gorilla/websocket"
)
type Point struct {
	X int	`json:"x"`
	Y int	`json:"y"`
}

type Game struct {
	Map []MapTile		`json:"map"`
	Player Character	`json:"players"`
	conn *websocket.Conn
}

type Character struct {
	Id int		`json:"id"`
	Loc Point	`json:"location"`
	Name string	`json:"name"`
	//Actions []Attack
}

type MapTile struct {
	Loc Point	`json:"loc"`
	Type string	`json:"type"`
}
