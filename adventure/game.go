package adventure

var games map[string]*Game

func init() {
	games = make(map[string]*Game)
}

func GetGame(id string) *Game {
	// sGame, ok := games[id]
	sGame, ok := games["main_game"]
	if ok {
		return sGame
	} else {
		game := &Game{}
		game.Init(9, 9)
		return game
	}
}

func (g *Game) Init(x, y int) {
	level := make([]MapTile, x * y)
	for i := 0; i < y; i++ {
		//level[i] = make([]int, x)
		for j := 0; j < x; j++ {
			if(i == 0 || j == 0 || i == x-1 || j == y-1) {
				level[i * x + j] = MapTile{Type: "wall", Loc: Point{X: j, Y: i}};
			} else {
				level[i * x + j] = MapTile{Type: "ground", Loc: Point{X: j, Y: i}};
			}
		}
	}
	players := Character{Loc: Point{X: x / 2, Y: y / 2}, Name: "player 1"}
	g.Map = level
	g.Player = players
}

func (g *Game) Move(id, x, y int) {
	g.Player.Loc.X = x
	g.Player.Loc.Y = y
}
