package adventure

import (
	"testing"
)

func TestGetCharacter(t *testing.T) {
	expected := Point{X:1, Y:1}
	game := Game{}
	game.Init(3,3)
	character := game.GetCharacter("1")
	if character != expected {
		t.Error("expected 1, 2 received ", character)
	}
}
